import React from 'react'
import { View, Button } from 'react-native'
import { Permissions, Notifications } from 'expo'
import styles from '../../styles'

export default class Settings extends React.Component {

  trigger () {
    return Permissions.askAsync(Permissions.NOTIFICATIONS).then(({ status }) => {
      console.log('permission status: ', status)
      if (status === 'granted') {
        const repeat = 'day'
        let time = new Date()
        time.setHours(18)
        time.setMinutes(0)
        console.log('time is: ', time)
        let schedulingOptions = {
          time: time,
          repeat: repeat
        }
        return Notifications.scheduleLocalNotificationAsync({
          title: 'How was your day?',
          body: '👋'
        },
        schedulingOptions)
      }
    })
  }
  render () {
    return (
      <View style={styles.container}>
        <Button title='Enable Daily Notification' onPress={this.trigger} />
      </View>
    )
  }
}
