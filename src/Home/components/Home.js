import React from 'react'
import { TouchableOpacity, Text, View, LayoutAnimation } from 'react-native'
import Calendar from '../../Calendar/Calendar'
import EmojiPicker from 'react-native-emoji-picker'
import { Ionicons } from '@expo/vector-icons'

import dayjs from 'dayjs'
import AppText from '../../../AppText'
import styles from '../../../styles'

export default class Home extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      today: dayjs().format('YYYY-MM-DD'),
      keyboardOpen: false
    }
  }

  render () {
    const { checkins, onCheckin } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <AppText>Today</AppText>
          <TouchableOpacity onPress={() => {
            LayoutAnimation.easeInEaseOut()
            this.setState({ keyboardOpen: true })
          }} style={{ paddingTop: 10 }}>
            <Text style={{ fontSize: 54 }}>{checkins[this.state.today] ? checkins[this.state.today] : <Ionicons size={54} name='ios-add-circle-outline' color='green' />}</Text>
          </TouchableOpacity>
          {
            this.state.keyboardOpen ? <EmojiPicker
              style={{ height: 300, backgroundColor: '#f4f4f4' }}
              hideClearButton
              onEmojiSelected={(emoji) => {
                LayoutAnimation.easeInEaseOut()
                onCheckin({
                  dateString: this.state.today,
                  emoji: emoji
                })
                this.setState({ keyboardOpen: false, finished: true })
              }} /> : <Calendar checkins={checkins} />
          }
        </View>
      </View >
    )
  }
}
