import React from 'react'
import { TouchableOpacity, View, LayoutAnimation, ScrollView } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Home from './Home'
import Settings from '../../Settings/Settings'
import Friends from '../../Friends/containers/Friends'
import AppText from '../../../AppText'
import styles from '../../../styles'

export default class Tabs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: 'me'
    }
  }
  tabChange = (tab) => {
    LayoutAnimation.easeInEaseOut()
    this.setState({ activeTab: tab })
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.container}>
          {
            this.state.activeTab === 'me' ? <Home /> : this.state.activeTab === 'friends' ? <Friends /> : <Settings />
          }
        </ScrollView>
        <View style={styles.tabBackground}>
          <TouchableOpacity onPress={() => this.tabChange('me')} style={styles.tab}><MaterialCommunityIcons size={22} name='face-profile' color={this.state.activeTab === 'me' ? '#00adf5' : 'black'} /><AppText style={[{ fontSize: 12 }, this.state.activeTab === 'me' ? styles.active : null]}>Me</AppText></TouchableOpacity>
          <TouchableOpacity onPress={() => this.tabChange('friends')} style={styles.tab}><MaterialCommunityIcons size={22} name='account-multiple' color={this.state.activeTab === 'friends' ? '#00adf5' : 'black'} /><AppText style={[{ fontSize: 12 }, this.state.activeTab === 'friends' ? styles.active : null]}>Friends</AppText></TouchableOpacity>
          <TouchableOpacity onPress={() => this.tabChange('settings')} style={styles.tab}><MaterialCommunityIcons size={22} name='settings' color={this.state.activeTab === 'settings' ? '#00adf5' : 'black'} /><AppText style={[{ fontSize: 12 }, this.state.activeTab === 'settings' ? styles.active : null]}>More</AppText></TouchableOpacity>
        </View>
      </View>
    )
  }
}
