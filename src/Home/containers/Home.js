import React from 'react'
import { connect } from 'react-redux'
import { ME_ADD_CHECKIN } from '../actions'
import HomeComponent from '../components/Home'

class Home extends React.Component {
  render () {
    const { checkins, onCheckin } = this.props

    return (
      <HomeComponent
        checkins={checkins}
        onCheckin={onCheckin}
      />
    )
  }
}

const mapStateToProps = state => ({
  checkins: state.checkins.me
})

const mapDispatchToProps = dispatch => {
  return {
    onCheckin: (checkin) => dispatch(ME_ADD_CHECKIN(checkin))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
