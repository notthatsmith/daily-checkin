export const ME_ADD_CHECKIN = checkin => ({
  type: 'ME_ADD_CHECKIN',
  dateString: checkin.dateString,
  emoji: checkin.emoji
})
