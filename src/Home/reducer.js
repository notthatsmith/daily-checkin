const checkins = (state = {
  me: [],
  friends: []
}, action) => {
  switch (action.type) {
    case 'ME_ADD_CHECKIN':
      let newCheckins = Object.assign({}, state.me)
      newCheckins[action.dateString] = action.emoji
      return Object.assign({}, state, {
        me: newCheckins
      })
    default:
      return state
  }
}

export default checkins
