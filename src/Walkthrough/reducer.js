const walkthrough = (state = {
  finished: false
}, action) => {
  switch (action.type) {
    case 'WALKTHROUGH_FINISH':
      return Object.assign({}, state, {
        finished: true
      })
    default:
      return state
  }
}

export default walkthrough
