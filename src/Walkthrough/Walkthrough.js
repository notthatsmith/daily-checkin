import React from 'react'
import { Text, View, TouchableOpacity, LayoutAnimation } from 'react-native'
import EmojiPicker from 'react-native-emoji-picker'
import { connect } from 'react-redux'
import dayjs from 'dayjs'
import { ME_ADD_CHECKIN } from '../Home/actions'
import { WALKTHROUGH_FINISH } from './actions'
import Tabs from '../Home/containers/Tabs'
import AppText from '../../AppText'
import styles from '../../styles'

class Walkthrough extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      today: dayjs().format('YYYY-MM-DD'),
      emoji: '👋',
      keyboardOpen: false
    }
  }
  render () {
    const { onCheckin, finished, onFinish } = this.props
    return (
      <View style={styles.container}>
        {
          !finished ? (
            <View style={styles.container}>
              <TouchableOpacity style={styles.border} onPress={() => {
                LayoutAnimation.easeInEaseOut()
                this.setState({ keyboardOpen: true })
              }
              }>
                {/* Would ❤ some animation to show that the emoji is clickable  */}
                <Text style={{ fontSize: 54 }}>{this.state.emoji}</Text>
              </TouchableOpacity>
              <AppText>How are you today?</AppText>
            </View>
          ) : finished === true ? <Tabs /> : null
        }
        {
          this.state.keyboardOpen ? <EmojiPicker
            style={{ height: 300, backgroundColor: '#f4f4f4' }}
            hideClearButton
            onEmojiSelected={(emoji) => {
              LayoutAnimation.easeInEaseOut()
              onCheckin({
                dateString: this.state.today,
                emoji: emoji
              })
              onFinish()
              this.setState({ keyboardOpen: false })
            }
            } /> : null
        }
      </View >
    )
  }
}

const mapStateToProps = state => ({
  checkins: state.checkins.me,
  finished: state.walkthrough.finished
})

const mapDispatchToProps = dispatch => {
  return {
    onCheckin: (checkin) => dispatch(ME_ADD_CHECKIN(checkin)),
    onFinish: () => dispatch(WALKTHROUGH_FINISH())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Walkthrough)
