import React from 'react'
import { connect } from 'react-redux'
import FriendComponent from '../Friends'

class Friends extends React.Component {
  render () {
    const { checkins } = this.props

    return (
      <FriendComponent
        checkins={checkins}
      />
    )
  }
}

const mapStateToProps = state => ({
  checkins: state.checkins.me
})

const mapDispatchToProps = dispatch => {
  return {
    // onAddFriend: () => dispatch(ME_ADD_CHECKIN(checkin))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Friends)
