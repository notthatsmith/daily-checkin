import React from 'react';
import { StyleSheet, Text, View, TextInput, NativeModules, LayoutAnimation, Button } from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker'
import AppText from '../../AppText'

export default class Remind extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timerPickerOpen: false
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Text style={{ fontSize: 54 }}>{this.props.emoji}</Text>
          <AppText>When is the best time to ask how you're doing?</AppText>
          <Button title='Open time' onPress={() => this.setState({ timerPickerOpen: true })} />
          <DateTimePicker
            isVisible={this.state.timerPickerOpen}
            onConfirm={(date) => {
              console.log(date)
              this.setState({ timerPickerOpen: false })
            }}
            onCancel={() => this.setState({ timerPickerOpen: false })}
            mode={'time'}
          />
        </View>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  }
});
