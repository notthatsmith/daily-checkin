import React from 'react'
import { Calendar as Cal } from 'react-native-calendars'
import { View } from 'react-native'
import AppText from '../../AppText'

export default class Calendar extends React.Component {
  renderDay = ({ date, state }) => {
    const today = state === 'today'
    const day = this.props.checkins[date.dateString]
    return (<View style={{ flex: 1 }}>
      {
        state !== 'disabled' ? <View>
          <AppText style={{ fontSize: 10, color: today ? '#00adf5' : 'black' }}>{date.day}</AppText>
          <AppText style={{ fontSize: 20 }}>{day}</AppText></View> : null
      }
    </View>)
  }

  render () {
    return (
      <Cal dayComponent={this.renderDay} />)
  }
}
