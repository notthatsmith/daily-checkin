import React from 'react'
import { NativeModules } from 'react-native'
import { Font } from 'expo'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import configureStore from './configureStore'
import Walkthrough from './src/Walkthrough/Walkthrough'

const { UIManager } = NativeModules
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
const { store, persistor } = configureStore()

export default class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      fontLoaded: false
    }
  }

  async componentDidMount () {
    await Font.loadAsync({
      'montserrat-regular': require('./assets/fonts/Montserrat-Regular.ttf')
    })
    this.setState({ fontLoaded: true })
  }

  render () {
    return this.state.fontLoaded === true ? (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Walkthrough />
        </PersistGate>
      </Provider>
    ) : null
  }
}
