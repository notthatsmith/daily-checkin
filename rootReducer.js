import { combineReducers } from 'redux'
import checkins from './src/Home/reducer'
import walkthrough from './src/Walkthrough/reducer'

export default combineReducers({
  checkins,
  walkthrough
})
