import { StyleSheet } from 'react-native';
export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  border: {
    marginBottom: 20,
    padding: 20,
    borderColor: '#ddd',
    borderWidth: 5,
    borderRadius: 160
  },
  tabBackground: {
    height: 49,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff'
  },
  tab: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  active: {
    color: '#00adf5'
  },
  friends: {
    marginTop: 20
  },
  friend: {
    paddingRight: 10,
    height: 10
  }
})
